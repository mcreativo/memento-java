public class ClassA
{
    private String attribue;

    public ClassA(String attribue)
    {
        this.attribue = attribue;
    }

    public void doSomething()
    {
        System.out.println("Doing something with attribute " + attribue);
    }

    public void setAttribue(String attribue)
    {
        this.attribue = attribue;
    }
}
